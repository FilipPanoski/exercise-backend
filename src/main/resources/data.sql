/*Switched to flyway*/

/*INSERT INTO user (country_code, username, email, gender, firstname, lastname, mdbid, deactivated, key_user_createuser, create_datetime, key_user_updateuser, update_datetime) VALUES ('1000', 'FPanoski123', 'filippanoski@gmail.com', 123, 'Filip', 'Panoski', '995411202143', TO_DATE('07/08/2018', 'DD/MM/YYYY'), 1, TO_TIMESTAMP('07/08/2018 11:32:24', 'DD/MM/YYYY HH24:MI:SS'), '1', TO_TIMESTAMP('07/08/2018 14:02:44', 'DD/MM/YYYY HH24:MI:SS'));
INSERT INTO user (country_code, username, email, gender, firstname, lastname, mdbid, deactivated, key_user_createuser, create_datetime, key_user_updateuser, update_datetime) VALUES ('1500', 'JohnDoe', 'john_doe@gmail.com', 123, 'John', 'Doe', '996368430185', TO_DATE('02/08/2018', 'DD/MM/YYYY'), 2, TO_TIMESTAMP('02/08/2018 09:42:24', 'DD/MM/YYYY HH24:MI:SS'), '1', TO_TIMESTAMP('05/08/2018 22:02:11', 'DD/MM/YYYY HH24:MI:SS'));
INSERT INTO userrights (userright, description) VALUES ('Create rights', 'Create posts');
INSERT INTO userrights (userright, description) VALUES ('View rights', 'View own and others posts');
INSERT INTO userrights (userright, description) VALUES ('Limited delete rights', 'Delete own posts');
INSERT INTO userrights (userright, description) VALUES ('Delete rights', 'Delete own and others posts');
INSERT INTO userroles (userrole, description) VALUES ('Admin', 'Administrative and user management privileges');
INSERT INTO userroles (userrole, description) VALUES ('Member', 'Creating and viewing posts');
INSERT INTO userrole_has_userrights (key_userrole, key_userright) VALUES (1, 1);
INSERT INTO userrole_has_userrights (key_userrole, key_userright) VALUES (1, 2);
INSERT INTO userrole_has_userrights (key_userrole, key_userright) VALUES (1, 3);
INSERT INTO userrole_has_userrights (key_userrole, key_userright) VALUES (1, 4);
INSERT INTO userrole_has_userrights (key_userrole, key_userright) VALUES (2, 1);
INSERT INTO userrole_has_userrights (key_userrole, key_userright) VALUES (2, 2);
INSERT INTO userrole_has_userrights (key_userrole, key_userright) VALUES (2, 3);
INSERT INTO user_has_roles (key_user, key_userrole, starttime, endtime, key_user_grantuser, granted_time) VALUES (1, 2, TO_TIMESTAMP('07/08/2018 11:59:55', 'DD/MM/YYYY HH24:MI:SS'), TO_TIMESTAMP('07/08/2019 12:00:59', 'DD/MM/YYYY HH24:MI:SS'), 1, TO_TIMESTAMP('07/08/2018 11:59:55', 'DD/MM/YYYY HH24:MI:SS'));
INSERT INTO user_has_roles (key_user, key_userrole, starttime, endtime, key_user_grantuser, granted_time) VALUES (1, 1, TO_TIMESTAMP('07/08/2018 12:00:11', 'DD/MM/YYYY HH24:MI:SS'), TO_TIMESTAMP('07/08/2019 12:01:11', 'DD/MM/YYYY HH24:MI:SS'), 2, TO_TIMESTAMP('07/08/2018 12:00:11', 'DD/MM/YYYY HH24:MI:SS'));
INSERT INTO user_has_roles (key_user, key_userrole, starttime, endtime, key_user_grantuser, granted_time) VALUES (2, 2, TO_TIMESTAMP('02/08/2018 09:50:13', 'DD/MM/YYYY HH24:MI:SS'), TO_TIMESTAMP('02/08/2019 09:51:13', 'DD/MM/YYYY HH24:MI:SS'), 3, TO_TIMESTAMP('02/08/2018 09:50:13', 'DD/MM/YYYY HH24:MI:SS'));*/