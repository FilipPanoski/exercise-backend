package com.filip.exercise.controller;

import com.filip.exercise.model.Role;
import com.filip.exercise.model.User;
import com.filip.exercise.model.UserRoles;
import com.filip.exercise.service.RoleService;
import com.filip.exercise.service.UserRolesService;
import com.filip.exercise.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/web-ui")
public class UserRolesController {

    private UserRolesService userRolesService;
    private UserService userService;
    private RoleService roleService;

    @Autowired
    public UserRolesController(UserRolesService userRolesService, UserService userService, RoleService roleService) {
        this.userRolesService = userRolesService;
        this.userService = userService;
        this.roleService = roleService;
    }

    @PutMapping("/add-role/users/{userId}/roles/{roleId}")
    public ResponseEntity<Role> addRoleToUser(@PathVariable Integer userId, @PathVariable Integer roleId) {
        User user = userService.getUserById(userId);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }
        Role role = roleService.getRoleById(roleId);
        if (role == null) {
            return ResponseEntity.notFound().build();
        }

        UserRoles newUserRoles = userRolesService.addRoleToUser(user, role);
        return ResponseEntity.ok(newUserRoles.getRole());
    }
}
