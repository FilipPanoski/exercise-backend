package com.filip.exercise.controller;

import com.filip.exercise.model.Right;
import com.filip.exercise.service.RightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/web-ui")
public class RightController {

    private RightService rightService;

    @Autowired
    public RightController(RightService rightService) {
        this.rightService = rightService;
    }

    @GetMapping("/rights")
    public ResponseEntity<List<Right>> getAllRights() {
        List<Right> rights = rightService.getAllRights();
        if (rights.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(rights);
    }

    @GetMapping("/roles/{roleId}/rights")
    public ResponseEntity<List<Right>> getAllRightsFromRole(@PathVariable Integer roleId) {
        List<Right> rights = rightService.getAllRightsByRoleId(roleId);
        if (rights.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(rights);
    }
}
