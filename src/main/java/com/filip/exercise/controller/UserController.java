package com.filip.exercise.controller;

import com.filip.exercise.dto.UserDto;
import com.filip.exercise.mapper.UserMapper;
import com.filip.exercise.model.User;
import com.filip.exercise.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/web-ui")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public ResponseEntity<List<UserDto>> getAllUsers() {
        List<User> users = userService.getAllUsers();
        if (users.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        List<UserDto> userDtos = new ArrayList<>();
        users.forEach(user -> userDtos.add(UserMapper.INSTANCE.userToUserDto(user)));
        return ResponseEntity.ok(userDtos);
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<UserDto> getUserById(@PathVariable Integer userId) {
        User user = userService.getUserById(userId);
        if (user == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(UserMapper.INSTANCE.userToUserDto(user));
    }

    @PostMapping("/users")
    public ResponseEntity<UserDto> createNewUser(@RequestBody UserDto user) {
        User newUser;
        try {
            newUser = UserMapper.INSTANCE.userDtoToUser(user);
        } catch (Exception ex) {
            return ResponseEntity.badRequest().build();
        }

        newUser = userService.createNewUser(newUser);
        return ResponseEntity.ok(UserMapper.INSTANCE.userToUserDto(newUser));
    }
}
