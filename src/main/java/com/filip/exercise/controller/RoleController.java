package com.filip.exercise.controller;

import com.filip.exercise.model.Role;
import com.filip.exercise.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/web-ui")
public class RoleController {

    private RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping("/roles")
    public ResponseEntity<List<Role>> getAllRoles() {
        List<Role> roles = roleService.getAllRoles();
        if (roles.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(roles);
    }

    @GetMapping("/roles/{roleId}")
    public ResponseEntity<Role> getRoleById(@PathVariable Integer roleId) {
        Role role = roleService.getRoleById(roleId);
        if (role == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(role);
    }

    @GetMapping("/users/{userId}/roles")
    public ResponseEntity<List<Role>> getAllRolesForUser(@PathVariable Integer userId) {
        List<Role> roles = roleService.getAllRolesByUserId(userId);
        if (roles.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(roles);
    }

    @PutMapping("/remove-right/rights/{rightId}/roles/{roleId}")
    public ResponseEntity<Role> removeRightFromRole(@PathVariable Integer rightId, @PathVariable Integer roleId) {
        Role role = roleService.getRoleById(roleId);
        if (role == null) {
            return ResponseEntity.notFound().build();
        }

        role = roleService.removeRightFromRole(rightId, role);
        return ResponseEntity.ok(role);
    }
}
