package com.filip.exercise.service;

import com.filip.exercise.model.Right;
import com.filip.exercise.repository.RightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RightService {

    private RightRepository rightRepository;

    @Autowired
    public RightService(RightRepository rightRepository) {
        this.rightRepository = rightRepository;
    }

    public List<Right> getAllRights() {
        return rightRepository.findAll();
    }

    public List<Right> getAllRightsByRoleId(Integer roleId) {
        return rightRepository.findRightsByRoleId(roleId);
    }
}
