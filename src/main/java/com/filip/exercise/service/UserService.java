package com.filip.exercise.service;

import com.filip.exercise.exception.ResourceAlreadyExistsException;
import com.filip.exercise.model.User;
import com.filip.exercise.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User getUserById(Integer userId) {
        return userRepository.findUserById(userId);
    }

    public User createNewUser(User newUser) {
        User user = userRepository.findUserByUsernameOrEmail(newUser.getUsername(), newUser.getEmail());
        if (user != null) {
            throw new ResourceAlreadyExistsException("User already exists.");
        }

        newUser.setDateTimeOfUserCreation(Calendar.getInstance());
        return userRepository.save(newUser);
    }
}
