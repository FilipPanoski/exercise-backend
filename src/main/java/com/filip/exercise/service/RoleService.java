package com.filip.exercise.service;

import com.filip.exercise.model.Right;
import com.filip.exercise.model.Role;
import com.filip.exercise.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

@Service
public class RoleService {

    private RoleRepository roleRepository;

    @Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public List<Role> getAllRoles() {
        return roleRepository.findAll();
    }

    public Role getRoleById(Integer roleId) {
        return roleRepository.findRoleById(roleId);
    }

    public List<Role> getAllRolesByUserId(Integer userId) {
        return roleRepository.findRolesByUser(userId);
    }

    public Role removeRightFromRole(Integer rightId, Role role) {
        Iterator<Right> iterator = role.getRights().iterator();
        while (iterator.hasNext()) {
            Right right = iterator.next();
            if (right.getId().equals(rightId)) {
                iterator.remove();
                break;
            }
        }
        return roleRepository.save(role);
    }
}
