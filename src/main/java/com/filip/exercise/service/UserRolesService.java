package com.filip.exercise.service;

import com.filip.exercise.exception.ResourceAlreadyExistsException;
import com.filip.exercise.model.Role;
import com.filip.exercise.model.User;
import com.filip.exercise.model.UserRoles;
import com.filip.exercise.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class UserRolesService {

    private UserRoleRepository userRoleRepository;


    @Autowired
    public UserRolesService(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    public UserRoles addRoleToUser(User user, Role role) {
        UserRoles userRoles = userRoleRepository.findUserRolesByUserAndRole(user, role);
        if (userRoles != null) {
            throw new ResourceAlreadyExistsException("User already has that role");
        }

        UserRoles newUserRoles = new UserRoles(user, role);
        newUserRoles.setStartTimeOfUserRoleCreation(Calendar.getInstance());
        return userRoleRepository.save(newUserRoles);
    }
}
