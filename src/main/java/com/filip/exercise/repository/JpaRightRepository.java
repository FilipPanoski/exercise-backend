package com.filip.exercise.repository;

import com.filip.exercise.model.Right;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JpaRightRepository extends RightRepository, PagingAndSortingRepository<Right, Integer> {

    List<Right> findAll();

    @Override
    @Query(value = "SELECT r.* FROM userrole_has_userrights u, userrights r WHERE u.key_userrole = ?1 " +
            "and u.key_userright = r.key_userright", nativeQuery = true)
    List<Right> findRightsByRoleId(Integer roleId);
}
