package com.filip.exercise.repository;

import com.filip.exercise.model.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JpaRoleRepository extends RoleRepository, PagingAndSortingRepository<Role, Integer> {

    List<Role> findAll();

    Role findRoleById(Integer id);

    @Override
    @Query(value = "SELECT ur.* FROM user_has_roles u, userroles ur WHERE u.key_user = ?1 " +
            "and u.key_userrole = ur.key_userrole", nativeQuery = true)
    List<Role> findRolesByUser(Integer id);
}
