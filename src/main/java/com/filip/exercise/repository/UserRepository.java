package com.filip.exercise.repository;

import com.filip.exercise.model.User;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface UserRepository {

    List<User> findAll();

    User findUserById(Integer userId);

    User findUserByUsernameOrEmail(String username, String email);

    User save(User user);
}
