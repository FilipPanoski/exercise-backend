package com.filip.exercise.repository;

import com.filip.exercise.model.Role;
import com.filip.exercise.model.User;
import com.filip.exercise.model.UserRoles;
import org.springframework.data.repository.NoRepositoryBean;


@NoRepositoryBean
public interface UserRoleRepository {

    UserRoles save(UserRoles userRoles);

    UserRoles findUserRolesByUserAndRole(User user, Role role);
}
