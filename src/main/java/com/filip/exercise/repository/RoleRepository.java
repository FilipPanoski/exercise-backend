package com.filip.exercise.repository;


import com.filip.exercise.model.Role;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface RoleRepository {

    List<Role> findAll();

    Role findRoleById(Integer id);

    List<Role> findRolesByUser(Integer id);

    Role save(Role role);
}
