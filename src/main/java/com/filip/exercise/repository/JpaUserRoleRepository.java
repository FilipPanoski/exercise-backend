package com.filip.exercise.repository;

import com.filip.exercise.model.Role;
import com.filip.exercise.model.User;
import com.filip.exercise.model.UserRoles;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface JpaUserRoleRepository extends UserRoleRepository, PagingAndSortingRepository<UserRoles, Integer> {

    UserRoles findUserRolesByUserAndRole(User user, Role role);
}
