package com.filip.exercise.repository;

import com.filip.exercise.model.Right;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;


@NoRepositoryBean
public interface RightRepository {

    List<Right> findAll();

    List<Right> findRightsByRoleId(Integer roleId);
}
