package com.filip.exercise.repository;

import com.filip.exercise.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface JpaUserRepository extends UserRepository, PagingAndSortingRepository<User, Integer> {

    List<User> findAll();

    User findUserById(Integer userId);

    User findUserByUsernameOrEmail(String username, String email);
}
