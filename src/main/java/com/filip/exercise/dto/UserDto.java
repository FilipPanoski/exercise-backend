package com.filip.exercise.dto;

import lombok.Data;

@Data
public class UserDto {

    private Integer id;

    private String countryCode;

    private String username;

    private String email;

    private String gender;

    private String firstName;

    private String lastName;

    private String mdbid;

}
