package com.filip.exercise.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_has_roles")
public class UserRoles {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "key_user_has_userrole")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "key_user")
    private User user;

    @OneToOne
    @JoinColumn(name = "key_userrole")
    private Role role;

    @Column(name = "starttime")
    private Timestamp startedTime;

    @Column(name = "endtime")
    private Timestamp endedTime;

    @Column(name = "key_user_grantuser")
    private Integer grantedUser;

    @Column(name = "granted_time")
    private Timestamp grantedTime;

    public UserRoles(User user, Role role) {
        this.user = user;
        this.role = role;
    }

    public void setStartTimeOfUserRoleCreation(Calendar date) {
        this.startedTime = new Timestamp(date.getTime().getTime());
    }
}
