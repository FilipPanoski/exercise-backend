package com.filip.exercise.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "userroles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "key_userrole")
    private Integer id;

    @Column(name = "userrole")
    private String userRole;

    @Column(name = "description")
    private String description;

    @ManyToMany
    @JoinTable(name = "userrole_has_userrights",
            joinColumns = @JoinColumn(name="key_userrole"),
            inverseJoinColumns = @JoinColumn(name = "key_userright"))
    private List<Right> rights;
}
