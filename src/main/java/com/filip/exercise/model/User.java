package com.filip.exercise.model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "key_user")
    private Integer id;

    @Column(name = "country_code")
    private String countryCode;

    @Column(name = "username")
    private String username;

    @Column(name = "email")
    private String email;

    @Column(name = "gender")
    private byte[] gender;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "mdbid")
    private String mdbid;

    @Column(name = "deactivated")
    private Date userDeactivated;

    @Column(name = "key_user_createuser")
    private Integer createdUser;

    @Column(name = "create_datetime")
    private Timestamp createdUserDateTime;

    @Column(name = "key_user_updateuser")
    private String updatedUser;

    @Column(name = "update_datetime")
    private Timestamp updatedUserDateTime;

    public User(String username, String email, String firstName, String lastName,
                String countryCode, String mdbid, byte[] gender) {

        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.countryCode = countryCode;
        this.mdbid = mdbid;
        this.gender = gender;
    }

    public void setDateTimeOfUserCreation(Calendar date) {
        this.createdUserDateTime = new Timestamp(date.getTime().getTime());
    }

}
