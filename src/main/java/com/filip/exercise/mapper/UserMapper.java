package com.filip.exercise.mapper;

import com.filip.exercise.dto.UserDto;
import com.filip.exercise.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.Base64;

@Mapper
public abstract class UserMapper {

    public static final UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "id", expression = "java(getId(userDto))")
    @Mapping(target = "firstName", expression = "java(getFirstName(userDto))")
    @Mapping(target = "lastName", expression = "java(getLastName(userDto))")
    @Mapping(target = "username", expression = "java(getUsername(userDto))")
    @Mapping(target = "email", expression = "java(getEmail(userDto))")
    @Mapping(target = "countryCode", expression = "java(getCountryCode(userDto))")
    @Mapping(target = "mdbid", expression = "java(getMdbid(userDto))")
    @Mapping(target = "gender", expression = "java(getGender(userDto))")
    public abstract User userDtoToUser(UserDto userDto);

    @Mapping(target = "id", expression = "java(getId(user))")
    @Mapping(target = "firstName", expression = "java(getFirstName(user))")
    @Mapping(target = "lastName", expression = "java(getLastName(user))")
    @Mapping(target = "username", expression = "java(getUsername(user))")
    @Mapping(target = "email", expression = "java(getEmail(user))")
    @Mapping(target = "countryCode", expression = "java(getCountryCode(user))")
    @Mapping(target = "mdbid", expression = "java(getMdbid(user))")
    @Mapping(target = "gender", expression = "java(getGender(user))")
    public abstract UserDto userToUserDto(User user);

    Integer getId(UserDto userDto) {
        return userDto.getId();
    }

    String getFirstName(UserDto userDto) {
        return userDto.getFirstName();
    }

    String getLastName(UserDto userDto) {
        return userDto.getLastName();
    }

    String getUsername(UserDto userDto) {
        return userDto.getUsername();
    }

    String getEmail(UserDto userDto) {
        return userDto.getEmail();
    }

    String getCountryCode(UserDto userDto) {
        return userDto.getCountryCode();
    }

    String getMdbid(UserDto userDto) {
        return userDto.getMdbid();
    }

    byte[] getGender(UserDto userDto) {
        return Base64.getDecoder().decode(userDto.getGender());
    }

    Integer getId(User user) {
        return user.getId();
    }

    String getFirstName(User user) {
        return user.getFirstName();
    }

    String getLastName(User user) {
        return user.getLastName();
    }

    String getUsername(User user) {
        return user.getUsername();
    }

    String getEmail(User user) {
        return user.getEmail();
    }

    String getCountryCode(User user) {
        return user.getCountryCode();
    }

    String getMdbid(User user) {
        return user.getMdbid();
    }

    String getGender(User user) {
        String gender =  Base64.getEncoder().encodeToString(user.getGender());
        if (gender.equals("bWFsZQ==")) {
            return "male";
        } else if (gender.contains("fem")) {
            return "female";
        }
        return gender;
    }
}
